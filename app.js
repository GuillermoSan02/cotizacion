const http = require("http");
const express = require("express");

const app = express();
app.set("view engine", "ejs");

const bodyparser = require("body-parser");

//para tomar valores de public (recursos)
app.use(express.static(__dirname + '/public/'));
app.use(bodyparser.urlencoded({extended:true}))

//arreglo de objetos "Listado"
app.get("/",(req, res)=>{
    res.render('index', {titulo: "Mi primer página en Embedded JavaScript", listado:datos});

});

//arreglo de objetos uwu
let datos=[{
    matricula: "2020030309",
    nombre: "Sanchez Lizarraga Guillermo Manuel",
    sexo: 'M',
    materias:["Ingles", "base de datos", "Tecnología I"]

},{
    matricula: "2020030309",
    nombre: "Sanchez Lizarraga Guillermo Manuel",
    sexo: 'M',
    materias:["Ingles", "base de datos", "Tecnología I"]

},{
    matricula: "2020030309",
    nombre: "Sanchez Lizarraga Guillermo Manuel",
    sexo: 'M',
    materias:["Ingles", "base de datos", "Tecnología I"]

}
]

//Tabla
app.get("/tabla",(req,res)=>{
    const params ={
        numero:req.query.numero //query para traer el valor
    }
    res.render('tabla', params);
})

app.post('/tabla', (req,res)=>{
    const params ={
        numero: req.body.numero
    }
    res.render("tabla", params);
})

//Cotizacion
app.get("/cotizacion",(req,res)=>{
    const params ={
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazo: req.query.plazo

    }
    res.render('cotizacion', params);
})

app.post('/cotizacion', (req,res)=>{
    const params ={
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazo: req.body.plazo
    }
    res.render("cotizacion", params);
})


//la página de erro va al final del get/post
app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html')
})

const puerto = 3000;
app.listen(puerto,()=>{
    console.log("Iniciando puerto")
});
